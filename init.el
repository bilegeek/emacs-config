(when (or (eq system-type 'gnu/linux) (eq system-type 'berkeley-unix))
  (modify-frame-parameters nil '((wait-for-wm . nil))))

(setq gc-cons-threshold most-positive-fixnum)

(defun my-minibuffer-setup-hook ()
  (setq gc-cons-threshold most-positive-fixnum))

(defun my-minibuffer-exit-hook ()
  (setq gc-cons-threshold 800000))

(add-hook 'minibuffer-setup-hook #'my-minibuffer-setup-hook)
(add-hook 'minibuffer-exit-hook #'my-minibuffer-exit-hook)

;;~~~~~

;;From https://emacs.stackexchange.com/questions/501/how-do-i-group-hooks
(defmacro hook-modes (modes &rest body)
  (declare (indent 1))
  `(--each ,modes
     (add-hook (intern (format "%s-hook" it))
               (lambda () ,@body))))

;;~~~~~

;;(setq debug-on-error t)

(defun reload-config()
  (interactive)
  (load-file "~/.emacs.d/init.el"))

(defun edit-config()
  (interactive)
  (find-file "~/.emacs.d/init.el"))
;;~~~~~

(prefer-coding-system 'utf-8-unix)
(when (display-graphic-p)
  (setq x-select-request-type '(UTF8_STRING COMPOUND_TEXT TEXT STRING)))
(set-language-environment "utf-8")
(set-default-coding-systems 'utf-8-unix)
(set-terminal-coding-system 'utf-8-unix)
(set-keyboard-coding-system 'utf-8-unix)

;;~~~~~

;;Text highlighting and crap.
(setq sentence-end-double-space nil)
(delete-selection-mode t)

;;Highlighting, visibility, etc.
(show-paren-mode 1)
(setq show-paren-delay nil)
(blink-cursor-mode 0)
(setq visible-bell t)
(customize-set-variable 'show-trailing-whitespace t)
(setq indicate-empty-lines t)
(global-linum-mode t)
(global-hl-line-mode)
(global-visual-line-mode 1)
(column-number-mode t)
(transient-mark-mode 1)
(setq word-wrap t)

(defun visual-line-line-range ()
    (save-excursion 
    (cons (progn (vertical-motion 0) (point)) 
          (progn (vertical-motion 1) (point)))))

(defun toggle-hl-line-range ()
    (interactive)
    (if (eq hl-line-range-function 'visual-line-line-range)
        (setq hl-line-range-function nil)
      (setq hl-line-range-function 'visual-line-line-range)))

;;Scrolling
(setq auto-window-vscroll nil)
(setq scroll-step 1)
(setq mouse-wheel-scroll-amount '(1 ((shift) . 1)))
(setq mouse-wheel-progressive-speed nil)
(setq mouse-wheel-follow-mouse 't)
(setq redisplay-dont-pause t)
(customize-set-variable 'mouse-yank-at-point t)
(global-set-key [mode-line C-mouse-1] 'tear-off-window)

;;Ignore case during completion.
(setq completion-ignore-case t)
(customize-set-variable 'read-file-name-completion-ignore-case t)
(customize-set-variable 'read-buffer-completion-ignore-case t)

;;Set tab widths.
(setq default-tab-width 3)
(setq-default c-basic-offset 3
	      tab-width 3
	      indent-tabs-mode t)

;;Title, modeline time, etc.

(setq inhibit-start-message t)
(setq inhibit-startup-screen t)
(setq initial-scratch-message nil)
(setq frame-title-format '(buffer-file-name "Emacs: %b (%f)" "Emacs: %b"))
(display-time)
;;(setq display-time-24hr-format t)
(setq display-time-day-and-date t)

;;Files, autosaving, etc.
(setq backup-directory-alist '(("." . "~/.emacs.d/backups")))
(save-place-mode 1)
(setq scroll-preserve-screen-position 'keep)
(setq default-directory "~/")
(setq vc-follow-symlinks t)

(when (or (eq system-type 'darwin) (eq system-type 'windows-nt))
  (setq delete-by-moving-to-trash t))

(when (eq system-type 'darwin)
  (ns-set-resource nil "ApplePressAndHoldEnabled" "NO"))

;;Commands
(setq disabled-command-function nil)
(fset 'yes-or-no-p 'y-or-n-p)

;;~~~~~

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Custom splitting functions ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun vsplit-last-buffer ()
  (interactive)
  (split-window-vertically)
  (other-window 1 nil)
  (switch-to-next-buffer))
(defun hsplit-last-buffer ()
  (interactive)
   (split-window-horizontally)
  (other-window 1 nil)
  (switch-to-next-buffer))

(global-set-key (kbd "C-x 2") 'vsplit-last-buffer)
(global-set-key (kbd "C-x 3") 'hsplit-last-buffer)

;;Next buffer and previous buffer key combos are loaded after Ivy.

(global-set-key (kbd "C--") 'text-scale-decrease)
(global-set-key (kbd "C-=") 'text-scale-increase)

(global-set-key (kbd "RET") 'newline-and-indent)

;;~~~~~

(defun backward-delete-word ()
  "Delete a word backwards. Delete text from previous line only when
current line is empty. This behaviour is similar to the one used by
SublimeText/Atom/VSCode/etc."
  (interactive)
  (if (= 0 (current-column))
      (call-interactively #'backward-delete-char-untabify)
    (let ((point-after-bw (save-excursion (backward-word) (point))))
      (if (< (count-lines 1 point-after-bw) (count-lines 1 (point)))
          (delete-region (line-beginning-position) (point))
        (delete-region (point) point-after-bw)))))

(defun goto-last-edit ()
  "Go to the last edit made in the current buffer."
  (interactive)
  (unless (or (consp buffer-undo-list)
              (not buffer-undo-list))
    (user-error "Can't go to last edit: invalid undo list"))
  (let ((pos (catch 'loop
               (dolist (item buffer-undo-list)
                 (when (and (consp item)
                            (or (integerp (car item))
                                (stringp (car item))))
                   (throw 'loop (abs (cdr item))))))))
    (unless (or (null pos)
                (= (point) pos))
      (push-mark)
      (goto-char pos))))

(global-set-key [(meta backspace)] 'goto-last-edit)
(global-set-key [(ctrl backspace)] 'backward-delete-word)

;;~~~~~

(defun xah-toggle-letter-case ()
  "Toggle the letter case of current word or text selection.
Always cycle in this order: Init Caps, ALL CAPS, all lower.

URL `http://ergoemacs.org/emacs/modernization_upcase-word.html'
Version 2017-04-19"
  (interactive)
  (let (
        (deactivate-mark nil)
        $p1 $p2)
    (if (use-region-p)
        (setq $p1 (region-beginning)
              $p2 (region-end))
      (save-excursion
        (skip-chars-backward "[:alnum:]-_")
        (setq $p1 (point))
        (skip-chars-forward "[:alnum:]-_")
        (setq $p2 (point))))
    (when (not (eq last-command this-command))
      (put this-command 'state 0))
    (cond
     ((equal 0 (get this-command 'state))
      (upcase-initials-region $p1 $p2)
      (put this-command 'state 1))
     ((equal 1  (get this-command 'state))
      (upcase-region $p1 $p2)
      (put this-command 'state 2))
     ((equal 2 (get this-command 'state))
      (downcase-region $p1 $p2)
      (put this-command 'state 0)))))

(global-set-key (kbd "C-9") 'xah-toggle-letter-case)

(defun xah-select-text-in-quote ()
  "Select text between the nearest left and right delimiters.
Delimiters here includes the following chars: \"<>(){}[]“”‘’‹›«»「」『』【】〖〗《》〈〉〔〕（）
This command select between any bracket chars, not the inner text of a bracket. For example, if text is

 (a(b)c▮)

 the selected char is “c”, not “a(b)c”.

URL `http://ergoemacs.org/emacs/modernization_mark-word.html'
Version 2016-12-18"
  (interactive)
  (let (
        ($skipChars
         (if (boundp 'xah-brackets)
             (concat "^\"" xah-brackets)
           "^\"<>(){}[]“”‘’‹›«»「」『』【】〖〗《》〈〉〔〕（）"))
        $pos
        )
    (skip-chars-backward $skipChars)
    (setq $pos (point))
    (skip-chars-forward $skipChars)
    (set-mark $pos)))

(global-set-key (kbd "M-9") 'xah-select-text-in-quote)

(defun xah-extend-selection ()
  "Select the current word, bracket/quote expression, or expand selection.
Subsequent calls expands the selection.

when no selection,
• if cursor is on a bracket, select whole bracketed thing including bracket
• if cursor is on a quote, select whole quoted thing including quoted
• if cursor is on the beginning of line, select the line.
• else, select current word.

when there's a selection, the selection extension behavior is still experimental.
Roughly:
• if 1 line is selected, extend to next line.
• if multiple lines is selected, extend to next line.
• if a bracketed text is selected, extend to include the outer bracket. If there's no outer, select current line.

 to line, or bracket/quoted text,
or text block, whichever is the smallest.

URL `http://ergoemacs.org/emacs/modernization_mark-word.html'
Version 2017-01-15"
  (interactive)
  (if (region-active-p)
      (progn
        (let (($rb (region-beginning)) ($re (region-end)))
          (goto-char $rb)
          (cond
           ((looking-at "\\s(")
            (if (eq (nth 0 (syntax-ppss)) 0)
                (progn
                  (message "left bracket, depth 0.")
                  (end-of-line) ; select current line
                  (set-mark (line-beginning-position)))
              (progn
                (message "left bracket, depth not 0")
                (up-list -1 t t)
                (mark-sexp))))
           ((eq $rb (line-beginning-position))
            (progn
              (goto-char $rb)
              (let (($firstLineEndPos (line-end-position)))
                (cond
                 ((eq $re $firstLineEndPos)
                  (progn
                    (message "exactly 1 line. extend to next whole line." )
                    (forward-line 1)
                    (end-of-line)))
                 ((< $re $firstLineEndPos)
                  (progn
                    (message "less than 1 line. complete the line." )
                    (end-of-line)))
                 ((> $re $firstLineEndPos)
                  (progn
                    (message "beginning of line, but end is greater than 1st end of line" )
                    (goto-char $re)
                    (if (eq (point) (line-end-position))
                        (progn
                          (message "exactly multiple lines" )
                          (forward-line 1)
                          (end-of-line))
                      (progn
                        (message "multiple lines but end is not eol. make it so" )
                        (goto-char $re)
                        (end-of-line)))))
                 (t (error "logic error 42946" ))))))
           ((and (> (point) (line-beginning-position)) (<= (point) (line-end-position)))
            (progn
              (message "less than 1 line" )
              (end-of-line) ; select current line
              (set-mark (line-beginning-position))))
           (t (message "last resort" ) nil))))
    (progn
      (cond
       ((looking-at "\\s(")
        (message "left bracket")
        (mark-sexp)) ; left bracket
       ((looking-at "\\s)")
        (message "right bracket")
        (backward-up-list) (mark-sexp))
       ((looking-at "\\s\"")
        (message "string quote")
        (mark-sexp)) ; string quote
       ((and (eq (point) (line-beginning-position)) (not (looking-at "\n")))
        (message "beginning of line and not empty")
        (end-of-line)
        (set-mark (line-beginning-position)))
       ((or (looking-back "\\s_" 1) (looking-back "\\sw" 1))
        (message "left is word or symbol")
        (skip-syntax-backward "_w" )
        ;; (re-search-backward "^\\(\\sw\\|\\s_\\)" nil t)
        (mark-sexp))
       ((and (looking-at "\\s ") (looking-back "\\s " 1))
        (message "left and right both space" )
        (skip-chars-backward "\\s " ) (set-mark (point))
        (skip-chars-forward "\\s "))
       ((and (looking-at "\n") (looking-back "\n" 1))
        (message "left and right both newline")
        (skip-chars-forward "\n")
        (set-mark (point))
        (re-search-forward "\n[ \t]*\n")) ; between blank lines, select next text block
       (t (message "just mark sexp" )
          (mark-sexp))
       ;;
       ))))

(global-set-key (kbd "M-8") 'xah-extend-selection)

(defun xah-select-line ()
  "Select current line. If region is active, extend selection downward by line.
URL `http://ergoemacs.org/emacs/modernization_mark-word.html'
Version 2017-11-01"
  (interactive)
  (if (region-active-p)
      (progn
        (forward-line 1)
        (end-of-line))
    (progn
      (end-of-line)
      (set-mark (line-beginning-position)))))

(global-set-key (kbd "M-7") 'xah-select-line)

(defun xah-select-block ()
  "Select the current/next block of text between blank lines.
If region is active, extend selection downward by block.

URL `http://ergoemacs.org/emacs/modernization_mark-word.html'
Version 2017-11-01"
  (interactive)
  (if (region-active-p)
      (re-search-forward "\n[ \t]*\n" nil "move")
    (progn
      (skip-chars-forward " \n\t")
      (when (re-search-backward "\n[ \t]*\n" nil "move")
        (re-search-forward "\n[ \t]*\n"))
      (push-mark (point) t t)
      (re-search-forward "\n[ \t]*\n" nil "move"))))

(global-set-key (kbd "M-6") 'xah-select-block)

;;~~~~~

(defun my-eww-open-in-new-buffer (url)
  "Fetch URL in a new EWW buffer."
  (interactiev
   (let* ((uris (eww-suggested-uris))
			 (prompt (concat "Enter URL or keywords"
								  (if uris (format " (default %s)" (car uris)) "")
								  ": ")))
     (list (read-string prompt nil nil uris))))
  (setq url (eww--dwim-expand-url url))
  (with-current-buffer
      (if (eq major-mode 'eww-mode) (clone-buffer)
        (generate-new-buffer "*eww*"))
    (unless (equal url (eww-current-url))
      (eww-mode)
      (eww (if (consp url) (car url) url)))))

;;~~~~~

;;From https://old.reddit.com/r/emacs/comments/cgptj7/weekly_tipstricketc_thread/
(defun describe-keymap (keymap)
  "Describe keys bound in KEYMAP."
  (interactive
   (list
    (completing-read "Keymap: "
							(cl-loop for x being the symbols
										if (and (boundp x) (keymapp (symbol-value x)))
										collect (symbol-name x))
							nil t nil 'variable-name-history)))
  (help-setup-xref (list #'describe-keymap keymap)
                   (called-interactively-p 'interactive))
  (with-output-to-temp-buffer (help-buffer)
    (princ keymap) (terpri) (terpri)
    (let ((doc (documentation-property
                (intern keymap)
                'variable-documentation)))
      (when doc (princ doc) (terpri) (terpri)))
    (princ (substitute-command-keys (format "\\{%s}" keymap)))))

;; Adapted from https://github.com/xuchunyang/emacs.d/blob/5b51f2783d9b22d284617ce9e7d1d7b518461599/lisp/chunyang-simple.el#L197-L212
(defun display-keys+ ()
  "Make keymap display readable."
  (interactive)
  (if (catch 'toggle-key-ov
        (dolist (o (overlays-in (point-min) (point-max)))
          (when (overlay-get o 'toggle-key-ov)
            (throw 'toggle-key-ov t))))
      (remove-overlays (point-min) (point-max) 'toggle-key-ov t)
    (save-excursion
      (goto-char (point-min))
      (while (re-search-forward "([0-9]+" nil :no-error)
        (let ((ov (make-overlay (1+ (match-beginning 0)) (match-end 0))))
          (overlay-put ov 'display (single-key-description
                                    (string-to-number (substring (match-string 0) 1))))
          (overlay-put ov 'toggle-key-ov t))))))

;;~~~~~

;;******************************
;;* NETWORK AND INITIALIZATION *
;;******************************

;; Don't try to use an external TLS program on Windows (it won't work).
(setq my-use-tls (or (not (eq system-type 'windows-nt)) (gnutls-available-p)))

(if (and (version< emacs-version "26.3") (>= libgnutls-version 30600))
    (setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3"))

;; Increase TLS security. To test this, run `test-https-verification' from `conf/utils/https'. See <https://lists.gnu.org/archive/html/emacs-devel/2018-06/msg00718.html>.
(setq network-security-level 'high)

(setq package-archives '(("gnu" . "https://elpa.gnu.org/packages/")
                         ("melpa" . "https://melpa.org/packages/")
								 ("melpa-stable" . "https://stable.melpa.org/packages/")
								 ("org" . "http://orgmode.org/elpa/")))

(package-initialize)

(add-to-list 'load-path "~/.emacs.d/elpa")
(cond
 ((eq system-type 'gnu/linux) (add-to-list 'load-path "~/.emacs.d/manualext/sly"))
 ((eq system-type 'windows-nt) (add-to-list 'load-path "~/sly")))

(setq load-prefer-newer t)

(require 'use-package)

;;~~~~~

;Version control porcelain.
;Apparently quite nice.
(require 'magit)
(setq magit-git-executable "git")
(setq git-commit-fill-column 9999)
(setq git-commit-summary-max-length 9999)
(setq git-commit-finish-query-functions nil)
(global-set-key (kbd "C-x g") 'magit-status)
;; use ido to checkout branchsego
(setq magit-completing-read-function 'magit-ido-completing-read)

;;~~~~~

(require 'git-gutter)
(global-git-gutter-mode t)
;; If you would like to use git-gutter.el and linum-mode
(git-gutter:linum-setup)

;; If you enable git-gutter-mode for some modes
;;(add-hook 'ruby-mode-hook 'git-gutter-mode)

(global-set-key (kbd "C-x C-g") 'git-gutter)
(global-set-key (kbd "C-x v =") 'git-gutter:popup-hunk)

;; Jump to next/previous hunk
(global-set-key (kbd "C-x p") 'git-gutter:previous-hunk)
(global-set-key (kbd "C-x n") 'git-gutter:next-hunk)

;; Stage current hunk
(global-set-key (kbd "C-x v s") 'git-gutter:stage-hunk)

;; Revert current hunk
(global-set-key (kbd "C-x v r") 'git-gutter:revert-hunk)

;; Mark current hunk
(global-set-key (kbd "C-x v SPC") #'git-gutter:mark-hunk)

;;~~~~~

(require 'git-gutter+)
(global-git-gutter+-mode)

(global-set-key (kbd "C-x g") 'git-gutter+-mode) ; Turn on/off in the current buffer
(global-set-key (kbd "C-x G") 'global-git-gutter+-mode) ; Turn on/off globally

(eval-after-load 'git-gutter+
  '(progn
   ;;; Jump between hunks
     (define-key git-gutter+-mode-map (kbd "C-x n") 'git-gutter+-next-hunk)
     (define-key git-gutter+-mode-map (kbd "C-x p") 'git-gutter+-previous-hunk)
    ;;; Act on hunks
     (define-key git-gutter+-mode-map (kbd "C-x v =") 'git-gutter+-show-hunk)
     (define-key git-gutter+-mode-map (kbd "C-x r") 'git-gutter+-revert-hunks)
   ;; Stage hunk at point.
   ;; If region is active, stage all hunk lines within the region.
     (define-key git-gutter+-mode-map (kbd "C-x t") 'git-gutter+-stage-hunks)
     (define-key git-gutter+-mode-map (kbd "C-x c") 'git-gutter+-commit)
     (define-key git-gutter+-mode-map (kbd "C-x C") 'git-gutter+-stage-and-commit)
     (define-key git-gutter+-mode-map (kbd "C-x C-y") 'git-gutter+-stage-and-commit-whole-buffer)
     (define-key git-gutter+-mode-map (kbd "C-x U") 'git-gutter+-unstage-whole-buffer)))

(require 'git-gutter-fringe+)

;; Optional: Activate minimal skin
(git-gutter-fr+-minimal)

;;~~~~~

(require 'mo-git-blame)

(autoload 'mo-git-blame-file "mo-git-blame" nil t)
(autoload 'mo-git-blame-current "mo-git-blame" nil t)

(global-set-key [?\C-c ?g ?c] 'mo-git-blame-current)
(global-set-key [?\C-c ?g ?f] 'mo-git-blame-file)

;;~~~~~

;;I've also installed git-timemachine; use M-x to access it.

;;~~~~~

(require 'projectile)
(projectile-mode +1)
(define-key projectile-mode-map (kbd "s-p") 'projectile-command-map)
(define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)

;;~~~~~

(require 'neotree)
(setq neo-smart-open nil)
(setq neo-autorefresh nil)
(setq neo-theme 'icons)
(setq neo-window-width 40)
(setq neo-smart-open t)
(setq projectile-switch-project-action 'neotree-projectile-action)

(defun neotree-project-dir ()
    "Open NeoTree using the git root."
    (interactive)
    (let ((project-dir (projectile-project-root))
          (file-name (buffer-file-name)))
      (neotree-toggle)
      (if project-dir
          (if (neo-global--window-exists-p)
              (progn
                (neotree-dir project-dir)
                (neotree-find file-name)))
        (message "Could not find git project root."))))

(global-set-key [f8] 'neotree-toggle)
;;(global-set-key [f8] 'neotree-project-dir)

(use-package treemacs
  :ensure t
  :defer t
  :init
  (with-eval-after-load 'winum
    (define-key winum-keymap (kbd "M-0") #'treemacs-select-window))
  :config
  (progn
    (setq treemacs-collapse-dirs                 (if (treemacs--find-python3) 3 0)
          treemacs-deferred-git-apply-delay      0.5
          treemacs-display-in-side-window        t
          treemacs-eldoc-display                 t
          treemacs-file-event-delay              5000
          treemacs-file-follow-delay             0.2
          treemacs-follow-after-init             t
          treemacs-git-command-pipe              ""
          treemacs-goto-tag-strategy             'refetch-index
          treemacs-indentation                   2
          treemacs-indentation-string            " "
          treemacs-is-never-other-window         nil
          treemacs-max-git-entries               5000
          treemacs-missing-project-action        'ask
          treemacs-no-png-images                 nil
          treemacs-no-delete-other-windows       t
          treemacs-project-follow-cleanup        nil
          treemacs-persist-file                  (expand-file-name ".cache/treemacs-persist" user-emacs-directory)
          treemacs-recenter-distance             0.1
          treemacs-recenter-after-file-follow    nil
          treemacs-recenter-after-tag-follow     nil
          treemacs-recenter-after-project-jump   'always
          treemacs-recenter-after-project-expand 'on-distance
          treemacs-show-cursor                   nil
          treemacs-show-hidden-files             t
          treemacs-silent-filewatch              nil
          treemacs-silent-refresh                nil
          treemacs-sorting                       'alphabetic-desc
          treemacs-space-between-root-nodes      t
          treemacs-tag-follow-cleanup            t
          treemacs-tag-follow-delay              1.5
          treemacs-width                         35)

    ;; The default width and height of the icons is 22 pixels. If you are
    ;; using a Hi-DPI display, uncomment this to double the icon size.
    ;;(treemacs-resize-icons 44)

    (treemacs-follow-mode t)
    (treemacs-filewatch-mode t)
    (treemacs-fringe-indicator-mode t)
    (pcase (cons (not (null (executable-find "git")))
                 (not (null (treemacs--find-python3))))
      (`(t . t)
       (treemacs-git-mode 'deferred))
      (`(t . _)
       (treemacs-git-mode 'simple))))
  :bind
  (:map global-map
        ("M-0"       . treemacs-select-window)
        ("C-x t 1"   . treemacs-delete-other-windows)
        ("C-x t t"   . treemacs)
        ("C-x t B"   . treemacs-bookmark)
        ("C-x t C-t" . treemacs-find-file)
        ("C-x t M-t" . treemacs-find-tag)))

(use-package treemacs-projectile
  :after treemacs projectile
  :ensure t)

(use-package treemacs-magit
  :after treemacs magit
  :ensure t)

(use-package treemacs-icons-dired
  :after treemacs dired
  :ensure t
  :config (treemacs-icons-dired-mode))

;;~~~~~

(require 'tramp)
(setq tramp-default-method "ssh")

;;~~~~~

(require 'org)
(add-to-list 'auto-mode-alist '("\\.org\\'" . org-mode))
(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-ca" 'org-agenda)

(defun ap/org-tree-to-indirect-buffer (&optional arg)
  "Create indirect buffer and narrow it to current subtree.
The buffer is named after the subtree heading, with the filename
appended.  If a buffer by that name already exists, it is
selected instead of creating a new buffer."
  (interactive "P")
  (let* ((new-buffer-p)
         (buffer-name (let ((heading (nth 4 (org-heading-components))))
                        ;; FIXME: I think there's an Org function that does this, but I can't find it.
                        (concat (if (string-match org-bracket-link-regexp heading)
                                    ;; Heading is an org link; use link name
                                    ;; TODO: but what if only part of the heading is?
                                    (match-string 3 heading)
                                  ;; Not a link; use whole heading
                                  heading)
                                "::" (buffer-name))))
         (new-buffer (or (get-buffer buffer-name)
                         (prog1 (condition-case nil
                                    (make-indirect-buffer (current-buffer) buffer-name 'clone)
                                  (error (make-indirect-buffer (current-buffer) buffer-name)))
                           (setq new-buffer-p t)))))
    (switch-to-buffer new-buffer)
    (when new-buffer-p
      (rename-buffer buffer-name)
      (org-narrow-to-subtree))))

(advice-add 'org-tree-to-indirect-buffer :override 'ap/org-tree-to-indirect-buffer)

;;~~~~~

;;(require 'auctex)

;;~~~~~

(require 'flyspell)

(cond ((eq system-type 'windows-nt) (setq ispell-program-name "c:/hunspell/bin/hunspell.exe")))

(setenv
 "DICPATH"
 (concat (getenv "HOME") "~/.emacs.d/dictionary"))

(dolist (hook '(text-mode-hook))
  (add-hook hook (lambda () (flyspell-mode 1))))

(dolist (hook '(change-log-mode-hook log-edit-mode-hook))
  (add-hook hook (lambda () (flyspell-mode -1))))

;;Enables Flyspell for comments and org-mode.
(add-hook 'prog-mode-hook 'flyspell-prog-mode)
(add-hook 'org-mode-hook 'turn-on-flyspell)

;;Stop ispell from criticizing org-mode stuff.
(add-to-list 'ispell-skip-region-alist '(":\\(PROPERTIES\\|LOGBOOK\\):" . ":END:"))
(add-to-list 'ispell-skip-region-alist '("#\\+BEGIN_SRC" . "#\\+END_SRC"))

;; NO spell check for embedded snippets
(defadvice org-mode-flyspell-verify (after org-mode-flyspell-verify-hack activate)
  (let* ((rlt ad-return-value)
         (begin-regexp "^[ \t]*#\\+begin_\\(src\\|html\\|latex\\|example\\|quote\\)")
         (end-regexp "^[ \t]*#\\+end_\\(src\\|html\\|latex\\|example\\|quote\\)")
         (case-fold-search t)
			b e)
    (when ad-return-value
      (save-excursion
        (setq b (re-search-backward begin-regexp nil t))
        (if b (setq e (re-search-forward end-regexp nil t))))
      (if (and b e (< (point) e)) (setq rlt nil)))
    (setq ad-return-value rlt)))

;;~~~~~

(require 'which-key)
(which-key-mode)
(which-key-setup-side-window-right-bottom)

;;~~~~~

(require 'undo-tree)
(global-undo-tree-mode)

;;~~~~~

(require 'powerline)
(powerline-default-theme)

;;~~~~~

(require 'erc)

;;~~~~~

(require 'ido)
(ido-mode 'buffers)
(setq ido-ignore-buffers '("^ " "*Completions*" "*Shell Command Output*"
									"*Messages*" "Async Shell Command" "*helm M-x*" "*scratch*"))

;;~~~~~

(require 'ivy)
(require 'swiper)
(require 'counsel)
(ivy-mode 1)
(setq ivy-use-virtual-buffers t
      ivy-count-format "%d/%d ")
(setq enable-recursive-minibuffers t)
;; enable this if you want `swiper' to use it
;;(setq search-default-mode #'char-fold-to-regexp)
(global-set-key "\C-s" 'swiper)
(global-set-key (kbd "C-c C-r") 'ivy-resume)
(global-set-key (kbd "<f6>") 'ivy-resume)
(global-set-key (kbd "M-x") 'counsel-M-x)
(global-set-key (kbd "C-x C-f") 'counsel-find-file)
(global-set-key (kbd "<f1> f") 'counsel-describe-function)
(global-set-key (kbd "<f1> v") 'counsel-describe-variable)
(global-set-key (kbd "<f1> l") 'counsel-find-library)
(global-set-key (kbd "<f2> i") 'counsel-info-lookup-symbol)
(global-set-key (kbd "<f2> u") 'counsel-unicode-char)
(global-set-key (kbd "C-c g") 'counsel-git)
(global-set-key (kbd "C-c j") 'counsel-git-grep)
(global-set-key (kbd "C-c k") 'counsel-ag)
(global-set-key (kbd "C-x l") 'counsel-locate)
(global-set-key (kbd "C-S-o") 'counsel-rhythmbox)
(define-key minibuffer-local-map (kbd "C-r") 'counsel-minibuffer-history)

;;Put this here because something else wouldn't override it.
(global-set-key (kbd "M-[") 'previous-buffer)
(global-set-key (kbd "M-]") 'next-buffer)

;;~~~~~

(require 'avy)
(global-set-key (kbd "C-c j") 'avy-goto-char)
(global-set-key (kbd "C-'") 'avy-goto-char-2)
(global-set-key (kbd "M-s") 'avy-goto-word-1)

;;~~~~~

(require 'all-the-icons)

(require 'all-the-icons-dired)
(add-hook 'dired-mode-hook 'all-the-icons-dired-mode)

(all-the-icons-ivy-setup)

(require 'ivy-rich)
(ivy-rich-mode 1)
(setcdr (assq t ivy-format-functions-alist) #'ivy-format-function-line)

;;~~~~~

(require 'yasnippet)
(yas-global-mode 1)
(yas-load-directory "~/.emacs.d/yasnippets")

;;~~~~~

(require 'ggtags)
(add-hook 'c-mode-common-hook
          (lambda ()
            (when (derived-mode-p 'c-mode 'c++-mode 'java-mode 'asm-mode)
              (ggtags-mode 1))))

(define-key ggtags-mode-map (kbd "C-c g s") 'ggtags-find-other-symbol)
(define-key ggtags-mode-map (kbd "C-c g h") 'ggtags-view-tag-history)
(define-key ggtags-mode-map (kbd "C-c g r") 'ggtags-find-reference)
(define-key ggtags-mode-map (kbd "C-c g f") 'ggtags-find-file)
(define-key ggtags-mode-map (kbd "C-c g c") 'ggtags-create-tags)
(define-key ggtags-mode-map (kbd "C-c g u") 'ggtags-update-tags)
(define-key ggtags-mode-map (kbd "M-,") 'pop-tag-mark)

;;~~~~~

(require 'company)
(setq company-quickhelp-delay 0.7
      company-tooltip-align-annotations t)

(global-company-mode)

(add-to-list 'company-backends '(company-lua company-math-symbols-latex company-math-symbols-unicode))

(define-key company-active-map (kbd "<up>") 'company-select-previous)
(define-key company-active-map (kbd "<down>") 'company-select-next)
(define-key company-active-map (kbd "\C-n") 'company-select-next)
(define-key company-active-map (kbd "\C-p") 'company-select-previous)
(define-key company-active-map (kbd "\C-d") 'company-show-doc-buffer)
(define-key company-active-map (kbd "M-.") 'company-show-location)

;;~~~~~

;; global activation of the unicode symbol completion 
(add-to-list 'company-backends 'company-math-symbols-unicode)

;;~~~~~

(require 'company-c-headers)
(add-to-list 'company-backends 'company-c-headers)

;;~~~~~

(require 'company-lua)

(defun my-lua-mode-company-init ()
  (setq-local company-backends '((company-lua
                                  company-etags
                                  company-dabbrev-code
                                  company-yasnippet))))
(add-hook 'lua-mode-hook #'my-lua-mode-company-init)

;;~~~~~

(require 'irony)
(use-package irony
				 :init
				 (add-hook 'c-mode-hook 'irony-mode)
				 (add-hook 'c++-mode-hook 'irony-mode)
				 (add-hook 'objc-mode-hook 'irony-mode)
				 (add-hook 'irony-mode-hook 'irony-cdb-autosetup-compile-options))

;;~~~~~

(require 'company-irony-c-headers)
;; Load with `irony-mode` as a grouped backend
(eval-after-load 'company
  '(add-to-list
    'company-backends '(company-irony-c-headers company-irony)))

;;~~~~~

(require 'cedet)

(setq semantic-default-submodes
      '(;; Perform semantic actions during idle time
        global-semantic-idle-scheduler-mode
        ;; Use a database of parsed tags
        global-semanticdb-minor-mode
        ;; Decorate buffers with additional semantic information
        global-semantic-decoration-mode
        ;; Highlight the name of the function you're currently in
        global-semantic-highlight-func-mode
        ;; show the name of the function at the top in a sticky
        global-semantic-stickyfunc-mode
        ;; Generate a summary of the current tag when idle
        global-semantic-idle-summary-mode
		global-semantic-idle-completions-mode
        ;; Show a breadcrumb of location during idle time
        global-semantic-idle-breadcrumbs-mode
        ;; Switch to recently changed tags with `semantic-mrub-switch-tags',
        ;; or `C-x B'
        global-semantic-mru-bookmark-mode))

(defvar semantic-modes
  '(emacs-lisp-mode python-mode c-mode objc-mode c++-mode js2-mode fortran-mode cobol-mode basic-mode ada-mode octave-mode haskell-mode opascal-mode lisp-mode prog-mode css-mode lua-mode))

(hook-modes semantic-modes
  (semantic-mode))

(global-ede-mode t)

;;~~~~~

;;Copied from some website, for JavaScript syntactic sugar.
(use-package js2-mode
  :mode "\\.js\\'"
  :config
  (setq js2-mode-show-parse-errors nil
        js2-mode-show-strict-warnings nil
        js2-basic-offset 2
        js-indent-level 2))

;;~~~~~

(use-package flycheck
  :ensure t
  :init (global-flycheck-mode))

;;~~~~~

(add-hook 'pascal-mode-hook 'opascal-mode)
(add-to-list 'auto-mode-alist '("\\.bas\\'" . basic-mode))
(add-to-list 'auto-mode-alist '("\\.cbl\\'" . cobol-mode))
(add-to-list 'auto-mode-alist '("\\.adb\\'" . ada-mode))

(defvar electric-pair-modes
  '(python-mode c-mode objc-mode c++-mode js2-mode fortran-mode cobol-mode basic-mode ada-mode inferior-octave-mode octave-mode haskell-mode opascal-mode prog-mode css-mode lua-mode))

(hook-modes electric-pair-modes
  (electric-pair-local-mode))

(require 'aggressive-indent)

(defvar aggressive-indent-modes
  '(python-mode c-mode objc-mode c++-mode js2-mode fortran-mode cobol-mode basic-mode ada-mode inferior-octave-mode octave-mode haskell-mode opascal-mode prog-mode css-mode lua-mode))

(hook-modes aggressive-indent-modes
  (aggressive-indent-mode))

;;~~~~~

(setq python-shell-interpreter "python3.7m")

;; Python mode
(defun my-merge-imenu ()
  (interactive)
  (let ((mode-imenu (imenu-default-create-index-function))
        (custom-imenu (imenu--generic-function imenu-generic-expression)))
    (append mode-imenu custom-imenu)))

(defun my-python-hooks()
    (interactive)
    (setq tab-width     3
          python-indent 3
          python-shell-interpreter "python3.6m"
          python-shell-interpreter-args "-i")
    (if (string-match-p "rita" (or (buffer-file-name) ""))
        (setq indent-tabs-mode t)
      (setq indent-tabs-mode nil)
    )
    (add-to-list
        'imenu-generic-expression
        '("Sections" "^#### \\[ \\(.*\\) \\]$" 1))
    (setq imenu-create-index-function 'my-merge-imenu)
    ;; pythom mode keybindings
    (define-key python-mode-map (kbd "M-.") 'jedi:goto-definition)
    (define-key python-mode-map (kbd "M-,") 'jedi:goto-definition-pop-marker)
    (define-key python-mode-map (kbd "M-/") 'jedi:show-doc)
    (define-key python-mode-map (kbd "M-?") 'helm-jedi-related-names)
    ;; end python mode keybindings

    (eval-after-load "company"
        '(progn
            (unless (member 'company-jedi (car company-backends))
                (setq comp-back (car company-backends))
                (push 'company-jedi comp-back)
                (setq company-backends (list comp-back)))
            )))

(add-hook 'python-mode-hook 'elpy-mode)
(add-hook 'python-mode-hook 'electric-pair-mode)
(add-hook 'inferior-python-mode-hook 'electric-pair-mode)
(add-hook 'python-mode-hook 'my-python-hooks)
;; End Python mode

(advice-add 'python-mode :before 'elpy-enable)

;;~~~~~

(require 'gnuplot)
(setq gnuplot-program "/usr/bin/gnuplot")

;;;; ====  Configure Gnuplot-mode  ====
;; these lines enable the use of gnuplot mode
  (autoload 'gnuplot-mode "gnuplot" "gnuplot major mode" t)
  (autoload 'gnuplot-make-buffer "gnuplot" "open a buffer in gnuplot mode" t)

;; this line automatically causes all files with the .gp extension to
;; be loaded into gnuplot mode
  (setq auto-mode-alist
     (append
        (list
           '("\\.gp$" . gnuplot-mode)
           '("\\.plt$" . gnuplot-mode)
        )
		  auto-mode-alist))

;;~~~~~

(remove-hook 'lisp-mode-hook 'slime-lisp-mode-hook)
(require 'sly)
(setq sly-auto-select-connection 'always)
(setq sly-kill-without-qureery-p t)
(setq sly-description-autofocus t)
(setq sly-inhibit-pipelining nil)
(setq sly-load-failed-fasl 'always)

(setq sly-mrepl-mode-hook 'company-mode)

(cond ((eq system-type 'gnu/linux) (setq sly-lisp-implementations
													  '((sbcl ("sbcl") :coding-system utf-8-unix)
														 (ccl ("ccl64") :coding-system utf-8-unix)
														 (cmucl ("lisp") :coding-system utf-8-unix)
														 (clisp ("clisp") :coding-system utf-8-unix)
														 (ecl ("ecl") :coding-system utf-8-unix))))
		((eq system-type 'windows-nt) (setq sly-lisp-implementations
														'((sbcl ("sbcl") :coding-system utf-8-unix)
														  (ccl32 ("c:/ccl/wx86cl.exe") :coding-system utf-8-unix)
														  (ccl64 ("c:/ccl/wx86cl64.exe") :coding-system utf-8-unix)
														  (clisp ("clisp") :coding-system utf-8-unix)))))
(setf sly-default-lisp 'sbcl)

;;(add-to-list 'minor-mode-alist '(sly-mode
;;                                 (" " sly--mode-line-format " ")))

;;~~~~~

(require 'geiser)
(setq geiser-active-implementations '(racket))

(cond ((eq system-type 'gnu/linux) (setq geiser-racket-binary "racket"))
		((eq system-type 'windows-nt) (setq geiser-racket-binary "c:/Program Files/Racket/Racket.exe")))

(setq geiser-repl-history-filename "~/.emacs.d/geiser-history")
(add-hook 'geiser-repl-mode-hook 'electric-pair-local-mode)

;;~~~~~

(require 'paredit)
(eval-when-compile (require 'cl))
(autoload 'enable-paredit-mode "paredit" "Turn on pseudo-structural editing of Lisp code." t)
(add-hook 'scheme-mode-hook           #'enable-paredit-mode)
(add-hook 'racket-mode-hook           'enable-paredit-mode)
(add-hook 'sly-mrepl-mode-hook 'enable-paredit-mode)
(add-hook 'racket-mode-hook      #'racket-unicode-input-method-enable)
(add-hook 'racket-repl-mode-hook #'racket-unicode-input-method-enable)
(add-hook 'lisp-mode-hook 'enable-paredit-mode)
(add-hook 'lisp-interaction-mode-hook 'enable-paredit-mode)
(add-hook 'eval-expression-minibuffer-setup-hook 'enable-paredit-mode)
(add-hook 'emacs-lisp-mode-hook 'enable-paredit-mode)
(add-hook 'eshell-mode-hook 'enable-paredit-mode)
(add-hook 'ielm-mode-hook 'enable-paredit-mode)

(put 'paredit-forward-delete 'delete-selection 'supersede)
(put 'paredit-backward-delete 'delete-selection 'supersede)
(put 'paredit-newline 'delete-selection t)

(add-hook 'sly-mrepl-mode-hook 'paredit-mode)
(defun override-sly-mrepl-bindings-with-paredit ()
  (define-key sly-mrepl-mode-map
      (read-kbd-macro paredit-backward-delete-key) nil))
(add-hook 'sly-mrepl-mode-hook 'override-sly-mrepl-bindings-with-paredit)

(add-hook 'sly-mrepl-mode-hook 'set-sly-mrepl-return)

(defun set-sly-mrepl-return ()
  (define-key sly-mrepl-mode-map (kbd "RET") 'sly-mrepl-return-at-end)
  (define-key sly-mrepl-mode-map (kbd "<return>") 'sly-mrepl-return-at-end))

(defun sly-mrepl-return-at-end ()
  (interactive)
  (if (<= (point-max) (point))
      (sly-mrepl-return)
	 (paredit-newline)))

(require 'paredit-menu)

;;~~~~~

;;From https://emacs.stackexchange.com/questions/1030/how-can-i-set-different-font-sizes-for-buffers-and-for-the-mode-line
;;But wrapped up in a function. Otherwise, when I would switch a theme, the font would go back.

(defun let-faces-be ()
  (interactive)
  (let ((faces '(mode-line
                 mode-line-buffer-id
                 mode-line-emphasis
                 mode-line-highlight
                 mode-line-inactive)))
    (mapc
     (lambda (face) (set-face-attribute face nil :font "DejaVu Sans Mono-9"))
     faces)))

(defvar *theme* 3)

(defun alternate-alternate-theme ()
  (interactive)
  (cond ((eq *theme* 0) (progn
								  (load-theme 'parchment)
								  (let-faces-be)
								  (setq *theme* 1)
								  (message "Loaded Parchment theme.")))
		  ((eq *theme* 1) (progn
								  (disable-theme 'parchment)
								  (load-theme 'tsdh-dark)
								  (let-faces-be)
								  (setq *theme* 2)
								  (message "Loaded tsdh-dark theme.")))
		  ((eq *theme* 2) (progn
								  (disable-theme 'tsdh-dark)
								  (load-theme 'ir-black)
								  (let-faces-be)
								  (setq *theme* 3)
								  (message "Loaded ir-black theme.")))
		  ((eq *theme* 3) (progn
								  (disable-theme 'ir-black)
								  (let-faces-be)
								  (setq *theme* 0)
								  (message "Loaded default theme.")))
		  (t (message "WTF did you do?"))))

(global-set-key [f12] 'alternate-alternate-theme)

;;~~~~~

;;THIS thing fixed a huge PITA I had with downloaded themes.
;;I couldn't switch themes cleanly! This takes care of that!
(setq custom-safe-themes t)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector
	["#000000" "#880000" "#005500" "#663311" "#004488" "#770077" "#007777" "#eeeecc"])
 '(custom-enabled-themes (quote (ir-black)))
 '(mouse-yank-at-point t)
 '(org-drill-done-count-color "#663311")
 '(org-drill-failed-count-color "#880000")
 '(org-drill-mature-count-color "#005500")
 '(org-drill-new-count-color "#004488")
 '(package-selected-packages
	(quote
	 (all-the-icons-ivy ivy-rich all-the-icons-dired all-the-icons treemacs-icons-dired treemacs-evil evil which-key aggressive-indent ir-black-theme counsel swiper ace-window treemacs-magit treemacs-projectile treemacs basic-mode parchment-theme whitespace-cleanup-mode gnuplot use-package undo-tree sqlup-mode racket-mode python-mode projectile powerline paredit-menu paradox pacmacs org neotree mo-git-blame magit jedi ipython-shell-send hippie-expand-slime helm-swoop helm-smex helm-company helm-c-yasnippet haskell-mode git-wip-timemachine git-timemachine git-gutter-fringe+ git-gutter ggtags geiser flycheck elpy ein cycle-themes company-shell company-rtags company-math company-lua company-jedi company-irony-c-headers company-irony company-c-headers company-auctex common-lisp-snippets color-theme cobol-mode avy autopair auto-complete-c-headers ada-mode ace-jump-mode ac-helm)))
 '(read-buffer-completion-ignore-case t)
 '(read-file-name-completion-ignore-case t)
 '(show-trailing-whitespace t))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;;~~~~~

(let-faces-be)

;;~~~~~

;; Put this at the very end of the init file.
;; Use a hook so the message doesn't get clobbered by other messages.
(add-hook 'emacs-startup-hook
          (lambda ()
            (message "Emacs ready in %s with %d garbage collections."
                     (format "%.2f seconds"
                             (float-time
                              (time-subtract after-init-time before-init-time)))
                     gcs-done)))
