;;; -*- no-byte-compile: t -*-
(define-package "treemacs-projectile" "20190619.1516" "Projectile integration for treemacs" '((projectile "0.14.0") (treemacs "0.0")) :commit "19788684fbdc17546ee2c4643a3488401f553fcd" :authors '(("Alexander Miller" . "alexanderm@web.de")) :maintainer '("Alexander Miller" . "alexanderm@web.de") :url "https://github.com/Alexander-Miller/treemacs")
