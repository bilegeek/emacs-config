;; Generated package description from wisi.el
(define-package "wisi" "2.1.0" "Utilities for implementing an indentation/navigation engine using a generalized LALR parser" '((cl-lib "1.0") (emacs "25.0") (seq "2.20")) :url "http://stephe-leake.org/ada/wisitoken.html" :keywords '("parser" "indentation" "navigation"))
