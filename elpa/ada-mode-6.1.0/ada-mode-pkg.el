;; Generated package description from ada-mode.el
(define-package "ada-mode" "6.1.0" "major-mode for editing Ada sources" '((wisi "2.1.0") (cl-lib "1.0") (emacs "25.0")) :url "http://www.nongnu.org/ada-mode/" :keywords '("languages" "ada"))
