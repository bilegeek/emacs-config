;;; -*- no-byte-compile: t -*-
(define-package "treemacs-magit" "20190619.1516" "Magit integration for treemacs" '((emacs "25.2") (treemacs "0.0") (pfuture "1.3") (magit "2.90.0")) :commit "19788684fbdc17546ee2c4643a3488401f553fcd" :authors '(("Alexander Miller" . "alexanderm@web.de")) :maintainer '("Alexander Miller" . "alexanderm@web.de") :url "https://github.com/Alexander-Miller/treemacs")
